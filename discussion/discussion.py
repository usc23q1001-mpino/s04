#mkdir <folder name1, folder name2>
#touch <filename>
#python3 <filename.py>

# Python has several structures to store collections
# or multiple items in a single variable

# Lists are similar to arrays in Javascript in a sense
# that they can contain a collection of data

# To create a list, the square bracket([]) is used.
names = ["Jhon", "Paul", "Angelica", "Zoro"]
programs = ['Pi-shape','short courses', 'Hello']
durations = [278,457,23]
truth_values = [True, False]

# A list can contain elements of different data types
sample_list = ["Apple", 3, True]
#print(sample_list)

# Getting the list size
# The number of elements in a list can be counted using
# len method()
#print(len(programs))

# Accessing values
# List can be accessed by providing the index number of the element
#print(programs[0])

# Access the last item
#print(programs[-1])

# Access the second item
#print(durations[1])

# Access the whole list
#print(durations)

# Access a range of values
# List [start ndx : end ndx]
#print(programs[0:2])
# Note that the end ndx is not included. (always -1)

# Updating List
# Print the current value
#print(f'Current value: {programs[2]}')

# Update the value
programs[2] = 'Calm Mind'

#print(f'New value: {programs[2]}')


# Mini-exercise
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing
#    in the following format:
#    The grade if John is 100
studNames = ['Angelo', 'Mike', 'Jane', 'Miah', 'Jesych']
studGrades = [80, 82, 79, 91, 87]

for n, g in zip(studNames, studGrades):
    print(f'The grade of {n} is {g}')


# List Manipulation
# List has methods that can be used to manipulate the elements within

# Adding List Items = the append() method allows to insert
# items to a list
programs.append('global')
print(programs)

# Deleting List Items = the "del" keyword can be used to 
# delete elements in the list
# Add new item to the duration list
durations.append(360)
print(durations)

# Delete the last item on the list
del durations[-1]
print(durations)

# Membership Checks = the "in" keyword check is the 
# elements is in the list
# returns TRUE or FALSE
print(23 in durations)
print(500 in durations)


# Sorting List = the sort() methods sorts the list 
# alphanumerically, ascending by default
print(names)
names.sort()
print(names)

# Emptying the List = the clear() method is used to empty
# the contents of the list
test_lists = [1, 2, 6, 4, 9]
print(test_lists)
test_lists.clear()
print(test_lists)

#########################################################

# Dictionaries are used to store data values in key:value pairs
# To create a dictionary, the curly braces({}) is used and
# the key-value pairs are denoted with (key : value)

person1 = {
	"name"        : "Mike",
	"age"         : 16,
	"occupation"  : "student",
	"isEnrolled"  : True,
	"subjects"    : ["Python", "SQL", "Django"]
}

print(person1)

# To get the number of key-value pairs in the dictionary,
# the len() method can be used
print(len(person1))

# Accessing values in the dictionary
print(person1["name"])

# The keys() method will return list of all the keys
# in the dictionary
print(person1.keys())

# The values() method will return list of all the value
# in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary
# as a key-value pair
print(person1.items())

# Adding key-value pairs can be done either putting a new
# index key and assigning a value or the update() method
person1["nationality"] = "Filipino"
person1.update({"fave_food" : "Sinigang"})
print(person1)

# Deleting entries can be done using the pop() method or the
# del keyword
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# clear() method empties the dictionary

# Looping through dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")


# Nested Dictionaries = disctionaries can be nested inside 
# each other
person3 = {
	"name"        : "Jesych",
	"age"         : 26,
	"occupation"  : "model",
	"isEnrolled"  : True,
	"subjects"    : ["Python", "SQL", "Django"]
}

classRoom = {
	"student1" : person1,
	"student3" : person3
}
print(classRoom)

# Mini-Exercise
# 1. Create a car dictionary with the following keys
#    (brand, model, year of make, color)
# 2. Print the following statement from the details:
#    "I own a <brand> <model> and it was made in <year of make>"

Car = {
	"brand"         :  "Toyota",
	"model"         :  "Supra",
	"year_of_make"  :  1993,
	"color"         :  "apple red"
}

print(f'I own a {Car["brand"]} {Car["model"]} and it was made in {Car["year_of_make"]}')

# Functions are blocks of code that runs when called
# The "def" keyword is used to create a function. The syntax is
# def <functionName>()

#define a function called my_greeting
def my_greeting():
	# code to be run when my_greeting is called back
	print("Hello User")

# Calling / Invoking a function
my_greeting()

# Parameters can be added to func to have more control
# to what the input for the func eill be
def greeting(username):
	#
	print(f'Hello,{username}!')

greeting("Bob")

# Return Statement = the "return" keyword allow func
# to return values
def addition(num1, num2):
	return num1 + num2

sum = addition(6, 20)
print(f'The sum is {sum}.')

# Lambda Function
# A lambda func is small anonymous func that can 
# be used for callbacks.print(greeting("Jesych"))

greeting = lambda person : f'Hello {person}'
print(greeting("Jesych"))
print(greeting("Young"))


# Classes 
# classes would serve as blueprints to describe the concepts
# of objects
# To create a class, the "class" keyword is used along with the class
# name that starts with an uppercase char
# class ClassName(): 

class Vehicle():
	# properties
	def __init__(self,brand,model,year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline"
		self.fuel_lvl = 0

	# methods
	def fill_fuel(self):
		print(f'Current fuel level: {self.fuel_lvl}')
		print('Filling up the fuel tank .....')
		self.fuel_lvl = 100
		print(f'New fuel level: {self.fuel_lvl}')

	def drive(self, distance):
		print(f'The car is driven {distance} kilometer/s.')
		print(f'The fuel level left: {self.fuel_lvl - distance}')

# Creating a new instance is done by calling the class
# and provide the arguments

new_car = Vehicle("Toyota", "Supra", "1993")

# Displaying attributes can be done using the dot notation
print(f'My car is a {new_car.brand} {new_car.model}')

# Calling methods of the instance
new_car.fill_fuel()

new_car.drive(50)





